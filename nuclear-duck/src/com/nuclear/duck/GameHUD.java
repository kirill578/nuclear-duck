package com.nuclear.duck;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.Entity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.color.Color;

import com.nuclear.duck.entities.Button;
import com.nuclear.duck.entities.interfaces.ButtonPressListener;
import com.nuclear.duck.utils.PlacementManager;

import android.graphics.Typeface;
import android.util.Log;

public class GameHUD extends HUD {
	
	private static GameHUD mInstance = null;
	private Text scoreText;
	private int mScore = 0;
	private float lastX;
	private float lastY;
	private PlacementManager pm;
	
	public static GameHUD getInstance(Engine mEngine,TextureProvider gtm,PlacementManager pm,PlacementManager cent_pm){
		if(mInstance != null)
			return mInstance;
		
		mInstance = new GameHUD(mEngine,gtm,pm,cent_pm);
		return mInstance;
	}
	
	/**
	 * Might return null
	 * @return
	 */
	public static GameHUD getInstance(){
		return mInstance;
	}
	
	private GameHUD(Engine mEngine,TextureProvider gtm,PlacementManager pm,final PlacementManager cent_pm){
		
		BitmapTextureAtlas mFontTexture = new BitmapTextureAtlas(mEngine.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        Font mFont = new Font(mEngine.getFontManager(), mFontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32, true, Color.BLACK);
        mEngine.getTextureManager().loadTexture(mFontTexture);
        mEngine.getFontManager().loadFont(mFont);
		
		scoreText = new Text(10, 5, mFont, "Score:0123456789", mEngine.getVertexBufferObjectManager());
		scoreText.setText("Score: 0");
		this.attachChild(scoreText);
		mScore = 0;
		
		this.pm = pm;
		
		final Button sellButton = new Button(this,gtm.getTexture(TextureProvider.SELL_BUTTON), 60, 220, mEngine.getVertexBufferObjectManager());
		final Button buildButton = new Button(this,gtm.getTexture(TextureProvider.BUILDING_BUTTON), 60, 100, mEngine.getVertexBufferObjectManager());
		
		final Button buildSamButton = new Button(this,gtm.getTexture(TextureProvider.SAM_BUTTON), 60, 220, mEngine.getVertexBufferObjectManager());
		final Button buildCentButton = new Button(this,gtm.getTexture(TextureProvider.CENTRIFUGE_BUTTON), 60, 100, mEngine.getVertexBufferObjectManager());
		buildSamButton.setVisible(false);
		buildCentButton.setVisible(false);
		
		buildButton.attachListener(new ButtonPressListener() {
			
			@Override
			public void onButtonPressed() {
				
				sellButton.setVisible(false);
				buildButton.setVisible(false);
				buildSamButton.setVisible(true);
				buildCentButton.setVisible(true);
				
				/*button.registerEntityModifier(new LoopEntityModifier(new SequenceEntityModifier(
		                new AlphaModifier(1.0f, 0.8f, 0.3f),
		                new AlphaModifier(1.0f, 0.3f, 0.8f)
		        )));*/		
			}
			
		});
		
		buildSamButton.attachListener(new ButtonPressListener() {
			@Override
			public void onButtonPressed() {
				GameHUD.this.pm.activate();
				
				sellButton.setVisible(true);
				buildButton.setVisible(true);
				buildSamButton.setVisible(false);
				buildCentButton.setVisible(false);
			}
		});
		
		buildCentButton.attachListener(new ButtonPressListener() {
			@Override
			public void onButtonPressed() {
				cent_pm.activate();
				
				sellButton.setVisible(true);
				buildButton.setVisible(true);
				buildSamButton.setVisible(false);
				buildCentButton.setVisible(false);
			}
		});
		
		
		//button.setScale(0.2f);
		//button.setAlpha(0.8f);
		
	}
	
	public void incrementScore(){
		mScore++;
		scoreText.setText("Score: " + Integer.toString(mScore) );
	}
	
	public void decrimentScore(){
		mScore--;
		scoreText.setText("Score: " + Integer.toString(mScore) );
	}

}
