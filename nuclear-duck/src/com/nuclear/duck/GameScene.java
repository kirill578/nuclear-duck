package com.nuclear.duck;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.andengine.engine.Engine;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.entities.Arena;
import com.nuclear.duck.entities.Bomber;
import com.nuclear.duck.entities.SamSite;
import com.nuclear.duck.entities.TwoLayerSamSite;
import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.Plane;
import com.nuclear.duck.utils.BomberFactory;
import com.nuclear.duck.utils.EntityManager;
import com.nuclear.duck.utils.FighterFactory;
import com.nuclear.duck.utils.MissileFactory;
import com.nuclear.duck.utils.PlacementManager;
import com.nuclear.duck.utils.SamFactory;
import com.nuclear.duck.utils.TwoLayerSamFactory;

public class GameScene extends Scene {
	
	private TextureProvider mGameTextureManager;
	public Engine mEngine; ////
	
	private ArrayList<AttackableEntity> mReactorMembers;
	private ArrayList<AttackableEntity> mAttackersMember;
	private Arena mArena;
	
	public GameScene(Engine e,TextureProvider gtm){
		super();
		
		mEngine = e;
		mGameTextureManager = gtm;
		EntityManager entityManager = new EntityManager(mEngine);
		
		//create the layers
		LayerManager.createLayers();
		this.attachChild(LayerManager.backgroundLayer);
		this.attachChild(LayerManager.groundLayer);
		this.attachChild(LayerManager.missileLayer);
		this.attachChild(LayerManager.topLayer);
		
		
		mArena = new Arena(gtm.getTexture(TextureProvider.BACKGROUND), mEngine, gtm, this); //
		this.registerTouchArea(mArena); //
		
		LayerManager.backgroundLayer.attachChild(mArena);
		
		
		
		
		
		
		
		
		
		
		
		VertexBufferObjectManager vbom = mEngine.getVertexBufferObjectManager();
		
		mReactorMembers = new ArrayList<AttackableEntity>();
		mAttackersMember = new ArrayList<AttackableEntity>();
		
		entityManager.addReferanceCollection(mReactorMembers);
		entityManager.addReferanceCollection(mAttackersMember);
		
		MissileFactory regularMissileFactory = new MissileFactory(gtm.getTexture(TextureProvider.MISSILE),vbom,entityManager);
		MissileFactory bombMissileFactory = new MissileFactory(gtm.getTexture(TextureProvider.BOMB),vbom,entityManager);
		
		
		BomberFactory bomberFactory = new BomberFactory(gtm.getTexture(TextureProvider.BOMBER), vbom, bombMissileFactory, mReactorMembers, mAttackersMember,entityManager,10.0f);
		FighterFactory F15Factory = new FighterFactory(gtm.getTexture(TextureProvider.F15), vbom, regularMissileFactory, mReactorMembers, mAttackersMember,entityManager,15.0f);
		FighterFactory F16Factory = new FighterFactory(gtm.getTexture(TextureProvider.F16), vbom, regularMissileFactory, mReactorMembers, mAttackersMember,entityManager,20.0f);
		
		Plane spritePlanef15 = F15Factory.build(25, -150);
		Plane spritePlanef16 = F16Factory.build(-20, -50);
		Bomber spritePlaneBomber = bomberFactory.build(-250, -200);
		Plane spritePlanef152 = F15Factory.build(350, -1050);
		Plane spritePlanef162 = F16Factory.build(0, -1200);
		
		for(int i=0;i<10;i++){
			LayerManager.topLayer.attachChild(F15Factory.build(2500 + i*250, -250 + i*25));
		}
		
		for(int i=0;i<6;i++){
			LayerManager.topLayer.attachChild(F16Factory.build(2500 + i*250, 250 + i*25));
		}
		
		for(int i=0;i<4;i++){
			LayerManager.topLayer.attachChild(bomberFactory.build(3000 + i*250, 750 + i*25));
		}
		
		//core
		SamSite samSite = new SamSite(gtm.getTexture(TextureProvider.CORE),regularMissileFactory, vbom, mAttackersMember,entityManager,450.0f);
		mReactorMembers.add(samSite);
		samSite.setCenterPosition(1200, 700);
		
		//setcamera incenter
		mEngine.getCamera().setCenter(1500, 1000);
		
		LayerManager.groundLayer.attachChild(samSite);
		
		MissileFactory SamMissileFactory = new MissileFactory(gtm.getTexture(TextureProvider.SAM_MISSILE),vbom,entityManager);
		
		/*//sam sites
		SamSite samSite2 = samSiteFactory.build(450, 180);
		SamSite samSite3 = samSiteFactory.build(450, 280);
		
		
		LayerManager.groundLayer.attachChild(samSite2);
		LayerManager.groundLayer.attachChild(samSite3);*/
		
		
		LayerManager.topLayer.attachChild(spritePlanef15);
		LayerManager.topLayer.attachChild(spritePlanef16);
		LayerManager.topLayer.attachChild(spritePlaneBomber);
		
		LayerManager.topLayer.attachChild(spritePlanef152);
		LayerManager.topLayer.attachChild(spritePlanef162);
		
		//debug drug plane
		//this.registerTouchArea(spritePlanef15);
		//this.registerTouchArea(spritePlanef16);
		//this.registerTouchArea(spritePlaneBomber);
		
		SamFactory samSiteFactory_centrefuge = new SamFactory(gtm.getTexture(TextureProvider.CENTRIFUGE), vbom, regularMissileFactory, mAttackersMember,mReactorMembers,entityManager,50.0f);
		SamFactory samSiteFactory_sam = new TwoLayerSamFactory(gtm.getTexture(TextureProvider.SAM_BASE),gtm.getTexture(TextureProvider.SAM_TOP),gtm.getTexture(TextureProvider.SAM_SHADOW), vbom, SamMissileFactory, mAttackersMember,mReactorMembers,entityManager,50.0f);
		
		PlacementManager pm_sam  = new PlacementManager(LayerManager.groundLayer, samSiteFactory_sam);
		mArena.attachTouchListener(pm_sam);
		PlacementManager pm_cent = new PlacementManager(LayerManager.groundLayer, samSiteFactory_centrefuge);
		mArena.attachTouchListener(pm_cent);
		
		mEngine.getCamera().setHUD(GameHUD.getInstance(e,gtm,pm_sam,pm_cent)); //
		
	}
	
	private void startMenuScene(){
		mEngine.setScene(new MenuScene(mEngine, mGameTextureManager));
	}
	
}
