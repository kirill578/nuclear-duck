package com.nuclear.duck;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class ImprovedSprite extends Sprite {
	
	public ImprovedSprite(float x,float y,TextureRegion texture,VertexBufferObjectManager vbom){
		super(x - texture.getWidth()/2 , y - texture.getHeight()/2,texture, vbom);
		setCullingEnabled(true);
		
		this.setPosition(x - getWidth()/2, y - getHeight()/2);
	}
	
	public float getCenterX(){
/*		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[0];*/
		return getX() + getScaleCenterX();
	}
	
	public float getCenterY(){
/*		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[1];*/
		return getY() + getScaleCenterY();
	}

}
