package com.nuclear.duck;

import org.andengine.entity.Entity;

public class LayerManager {

	public static void createLayers(){
		backgroundLayer = new Entity();
		groundLayer = new Entity();
		missileLayer = new Entity();
		topLayer = new Entity();
	}
	
	public static Entity backgroundLayer;
	
	public static Entity groundLayer;

	public static Entity missileLayer;
	
	public static Entity topLayer;
	
}
