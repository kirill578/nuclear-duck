package com.nuclear.duck;

import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import android.app.PendingIntent;

public class MainActivity extends SimpleBaseGameActivity {
	
	public static final int CAMERA_WIDTH = 800;
	public static final int CAMERA_HEIGHT = 480;
	
	private Camera mCamera;
	private Scene mScene;
	
	public enum SceneType {
		SPLASH,
		MENU,
		OPTIONS,
		GAME,
	}
	 
	public SceneType currentScene = SceneType.SPLASH;
	
	private TextureProvider mGameTextureManager;
	
	public EngineOptions onCreateEngineOptions() {
		mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		RatioResolutionPolicy ratio = new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, ratio, mCamera);
	}

	@Override
	protected void onCreateResources() {
		mGameTextureManager = new TextureProvider(getTextureManager(),this);
		mGameTextureManager.loadTextures();
	}

	@Override
	protected Scene onCreateScene() {
		mEngine.registerUpdateHandler(new FPSLogger());
		mScene = new MenuScene(mEngine, mGameTextureManager);
		//mScene = new GameScene(mEngine, mGameTextureManager);
		return mScene;
	}
}
