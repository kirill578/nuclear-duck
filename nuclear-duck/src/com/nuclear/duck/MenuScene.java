package com.nuclear.duck;

import org.andengine.engine.Engine;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;

import android.util.Log;

public class MenuScene extends Scene {
	
	private TextureProvider mGameTextureManager;
	Engine mEngine;

	public MenuScene(Engine e,TextureProvider gtm){
		super();
		mGameTextureManager = gtm;
		mEngine = e;
		this.setBackground(new SpriteBackground(new Sprite(0, 0, mGameTextureManager.getTexture(TextureProvider.MENU), mEngine.getVertexBufferObjectManager())));
		
		Sprite bStart = new Sprite( -30, 150 ,mGameTextureManager.getTexture(TextureProvider.MENU_START_GAME), mEngine.getVertexBufferObjectManager()){
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
				startGameScene();
				return true;
			}
			
		};
		
		Sprite bSettings = new Sprite( -30 , 280 ,mGameTextureManager.getTexture(TextureProvider.MENU_SETTINGS), mEngine.getVertexBufferObjectManager());
		Sprite bFacebook = new Sprite( 800 -70 , 480 -70 ,mGameTextureManager.getTexture(TextureProvider.MENU_FACEBOOK), mEngine.getVertexBufferObjectManager());
		
		final Sprite duck = new Sprite( 400 , 70 ,mGameTextureManager.getTexture(TextureProvider.DUCK_BIG), mEngine.getVertexBufferObjectManager());
		
		final LoopEntityModifier scaleInOutModifier = new LoopEntityModifier(
		        new SequenceEntityModifier(
		                new ScaleModifier(3.0f, 0.85f, 0.9f),
		                new ScaleModifier(3.0f, 0.9f, 0.85f)
		        )
		);
		
		duck.registerEntityModifier(scaleInOutModifier);
		
		this.registerTouchArea(bStart);
		this.attachChild(duck);
		this.attachChild(bStart);
		this.attachChild(bSettings);
		this.attachChild(bFacebook);
	}
	
	private void startGameScene(){
		Log.d("Game", "start gameScence");
		mEngine.setScene(new GameScene(mEngine, mGameTextureManager));
	}
	
}
