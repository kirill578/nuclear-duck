package com.nuclear.duck;

import java.util.ArrayList;

import org.andengine.engine.Engine;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;

import android.content.Context;

public class TextureProvider {
	
	public final static int SPLASH = 0;
	
	public final static int MENU = 1;
	public final static int MENU_START_GAME = 2;
	public final static int MENU_SETTINGS = 3;
	public final static int MENU_FACEBOOK = 4;
	
	public final static int CORE = 5;
	public final static int MISSILE = 6;
	public final static int BUILDING = 7;
	public final static int BACKGROUND = 8;
	public final static int F15 = 9;
	public final static int F16 = 11;
	
	public final static int BOMBER = 12;
	
	public final static int DUCK_BIG = 10;
	
	public final static int BOMB = 13;
	
	public final static int CENTRIFUGE = 14;
	
	public final static int BUILDING_BUTTON = 15;
	public final static int SELL_BUTTON = 16;
	
	public final static int SAM_BASE = 17;
	public final static int SAM_TOP = 18;
	public final static int SAM_MISSILE = 19;
	
	public final static int CENTRIFUGE_BUTTON = 20;
	
	public final static int SAM_BUTTON = 21;
	
	public final static int SAM_SHADOW = 22;
	
	public final static String[] PATH = {
		"menu_background.png",
		"menu_background.png",
		"menu_start_game.png",
		"menu_settings.png",
		"menu_facebook.png",
		
		"core.png",
		"missile.png",
		"building.png",
		"background.png",
		"plane.png",
		
		"duck_big.png",
		
		"F16.png",
		
		"Bomber.png",
		
		"bomb.png",
		
		"centrifuge.png",
		
		"BuildingButton.png",
		
		"SellButton.png",
		
		"sam_base.png",
		
		"sam_top.png",
		
		"sam_missile.png",
		
		"CentrifugeButton.png",
		
		"SamButton.png",
		
		"sam_shadow.png"
	};
	
	private Context mContext;
	private ArrayList<TextureRegion> mTextures;
	private ArrayList<BitmapTextureAtlas> mTextureAtlases;
	private TextureManager mTextureManager;
	
	public void loadTextures(){
		load(800,480,SPLASH);
		load(800,480,MENU);
		
		load(426,122,MENU_START_GAME);
		load(426,122,MENU_SETTINGS);
		
		load(56,59,MENU_FACEBOOK);
		
		load(200,200,CORE); 
		load(117,29,MISSILE);
		load(289,404,BUILDING); 
		load(800,480,BACKGROUND); 
		load(337,409,F15); 
		
		load(370,437,DUCK_BIG); 
		
		load(241,334,F16);
		
		load(568,245,BOMBER);
		
		load(70,35,BOMB);
		
		load(100,100,CENTRIFUGE);
		
		load(104,104,BUILDING_BUTTON);
		
		load(104,104,SELL_BUTTON);
		
		load(100,100,SAM_BASE);
		load(100,140,SAM_TOP);
		load(37,9,SAM_MISSILE);
		
		load(104,104,CENTRIFUGE_BUTTON);
		load(104,104,SAM_BUTTON);
		
		load(100,140,SAM_SHADOW);
	}
	
	public TextureProvider(TextureManager tm,Context ctx){
		mTextureManager = tm;
		mContext = ctx;
		mTextures = new ArrayList<TextureRegion>();
		mTextureAtlases = new ArrayList<BitmapTextureAtlas>();
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
	}
	
	public void loadSplash(){
		
	}
	
	public void getSplash(){
		
	}
	
	
	private void load(int width,int height,int id){
		BitmapTextureAtlas temp = new BitmapTextureAtlas(mTextureManager, width, height,TextureOptions.BILINEAR);
		mTextureAtlases.add(temp);
		mTextures.add(BitmapTextureAtlasTextureRegionFactory.createFromAsset(temp, mContext, PATH[id], 0, 0));
		mTextureManager.loadTexture(temp);
	}

	public TextureRegion getTexture(int id){
		return mTextures.get(id);
	}
	
}
