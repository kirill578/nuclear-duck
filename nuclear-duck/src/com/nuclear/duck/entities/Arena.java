package com.nuclear.duck.entities;

import java.util.ArrayList;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.list.IntArrayList;

import com.nuclear.duck.LayerManager;
import com.nuclear.duck.TextureProvider;
import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.BasicEntity;
import com.nuclear.duck.entities.interfaces.Plane;
import com.nuclear.duck.entities.interfaces.TouchListener;
import com.nuclear.duck.utils.BomberFactory;
import com.nuclear.duck.utils.FighterFactory;
import com.nuclear.duck.utils.MissileFactory;
import com.nuclear.duck.utils.SamFactory;

import android.util.Log;

public class Arena extends Entity implements ITouchArea {
	
	private Camera mCamera;
	private Engine mEngine;
	private Scene mGameScane;
	
	private float lastX,lastY;
	private boolean wentTroughtDown = false;
	private ArrayList<AttackableEntity> mReactorMembers;
	private ArrayList<AttackableEntity> mAttackersMember;

	public Arena(TextureRegion texture,Engine engine,TextureProvider gtm,Scene scene){
		super(0,0);
		for(int i = 0 ; i < 3 ; i++)
			for(int j = 0 ; j < 3 ; j++)
					this.attachChild(new Sprite(i*texture.getWidth() , j*texture.getHeight(), texture, engine.getVertexBufferObjectManager()));
		mCamera = engine.getCamera();
		mEngine = engine;
		mGameScane = scene;
		mTouchListeners = new ArrayList<TouchListener>();
	}
	
	private ArrayList<TouchListener> mTouchListeners;
	
	public void attachTouchListener(TouchListener l){
		mTouchListeners.add(l);
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
		
		if (pSceneTouchEvent.isActionDown() || (!wentTroughtDown && pSceneTouchEvent.isActionMove())) {
			lastX = pSceneTouchEvent.getMotionEvent().getX();
			lastY = pSceneTouchEvent.getMotionEvent().getY();
			wentTroughtDown = true;
			
			for(TouchListener l : mTouchListeners )
				l.onTouch(pSceneTouchEvent.getX() , pSceneTouchEvent.getY());
			
		}
		else
		if(pSceneTouchEvent.isActionMove()){
			float deltaX = pSceneTouchEvent.getMotionEvent().getX() - lastX;
			float deltaY = pSceneTouchEvent.getMotionEvent().getY() - lastY;
			lastX = pSceneTouchEvent.getMotionEvent().getX();
			lastY = pSceneTouchEvent.getMotionEvent().getY();
			mCamera.setCenter(mCamera.getCenterX() - deltaX ,mCamera.getCenterY() - deltaY);
		}
		else
		if(pSceneTouchEvent.isActionUp()){
			float deltaX = pSceneTouchEvent.getMotionEvent().getX() - lastX;
			float deltaY = pSceneTouchEvent.getMotionEvent().getY() - lastY;
			mCamera.setCenter(mCamera.getCenterX() - deltaX ,mCamera.getCenterY() - deltaY);
			wentTroughtDown = false;
		}
		
		return true;
	}

	@Override
	public boolean contains(float pX, float pY) {
		return true;
	}

}
