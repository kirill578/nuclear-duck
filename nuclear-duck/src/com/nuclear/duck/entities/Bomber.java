package com.nuclear.duck.entities;

import java.util.ArrayList;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.AttackingEntity;
import com.nuclear.duck.entities.interfaces.MortalEntity;
import com.nuclear.duck.entities.interfaces.MovableEntity;
import com.nuclear.duck.entities.interfaces.NoTargetException;
import com.nuclear.duck.entities.interfaces.Plane;
import com.nuclear.duck.utils.EntityManager;
import com.nuclear.duck.utils.MissileFactory;

import android.util.Log;

public class Bomber extends Plane implements MortalEntity,MovableEntity,AttackableEntity,AttackingEntity {
		
	public Bomber(TextureRegion texture, MissileFactory mf,
			VertexBufferObjectManager vbom, ArrayList<AttackableEntity> targets,EntityManager em,float life) {
		super(texture, mf, vbom, targets,em,life);
		
		this.mAttackInterval = 300l;
		this.mMinimumAttackDistance = 0.0f;
	}


	@Override
	public void update() {
		
		float vectorTarget;
		AttackableEntity closest = null;
		
		try {
			closest = this.getClosest();
			//get Vector to closest target
			vectorTarget = (float)( Math.atan2(this.getCenterY() - closest.getCenterY(),
					this.getCenterX() - closest.getCenterX()) + Math.PI);
		} catch (NoTargetException e) {
			//try to catch itself
			vectorTarget = (float) (this.getSpeedVector() + Math.PI/800.0f);
		}
		
		float delta = (float) (this.getSpeedVector() - vectorTarget );
		
		while (delta < -Math.PI) delta += 2.0f * Math.PI;
		while (delta > Math.PI) delta -= 2.0f * Math.PI;
		
		
		//go over the top of the building
		if( delta >= 2.0f * Math.PI * 0.002 ) delta = (float) (2.0f * Math.PI * 0.002);
		if( delta <= - 2.0f * Math.PI * 0.002 ) delta = (float) (-2.0f * Math.PI * 0.002);
		
		this.setSpeedByVector(1.2f, this.getSpeedVector() - delta);
		
		//attack
		if(closest != null)
			this.Attack(closest);
		
		//move
		this.updateLocation();
	}

	
	@Override
	protected IEntity getAttackingMissile(AttackableEntity target) {
		return this.mMissileFactory.buildBomb(this.getCenterX(), this.getCenterY(), this.getSpeedVector(),target);
	}


	@Override
	public float getDamage() {
		return 0;
	}
	
}
