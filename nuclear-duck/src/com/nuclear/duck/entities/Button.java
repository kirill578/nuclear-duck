package com.nuclear.duck.entities;

import java.util.ArrayList;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import com.nuclear.duck.entities.interfaces.BasicEntity;
import com.nuclear.duck.entities.interfaces.ButtonPressListener;

public class Button extends Sprite implements BasicEntity {
	
	ArrayList<ButtonPressListener> mBPLs;
	
	public Button(Scene scene,TextureRegion texture,int centerX,int centerY,VertexBufferObjectManager vbom){
		super(0 , 0, texture, vbom);
		this.setCenterPosition(centerX, centerY);
		mBPLs = new ArrayList<ButtonPressListener>();
		scene.registerTouchArea(this);
		scene.attachChild(this);
	}
	
	public void attachListener(ButtonPressListener bpl){
		mBPLs.add(bpl);
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
		if(pSceneTouchEvent.isActionUp() && isVisible()){
			
			for( ButtonPressListener listner : mBPLs )
				listner.onButtonPressed();
			
			return true;
		} else {
			return false;
		}
	}

	public float getCenterX() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[0];
	}

	public float getCenterY() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[1];
	}

	@Override
	public void setCenterPosition(float x, float y) {
		this.setPosition( x - this.getWidth()/2 , y - this.getHeight()/2 );
	}

	@Override
	public void consume() {
		// TODO Auto-generated method stub
	}

	public float getTrueWidth() {
		return this.getWidthScaled();
	}

	public float getTrueHeight() {
		return this.getHeightScaled();
	}
	
	@Override
	public float getDistance(BasicEntity e) {
		return 0;
	}

	@Override
	public float getTrueDistance(BasicEntity e) {
		return 0;
	}

	@Override
	public boolean contains(float pX, float pY) {
		return super.contains(pX, pY) && this.isVisible();
	}
	

}
