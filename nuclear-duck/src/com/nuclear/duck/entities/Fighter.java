package com.nuclear.duck.entities;

import java.util.ArrayList;

import org.andengine.entity.IEntity;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.NoTargetException;
import com.nuclear.duck.entities.interfaces.Plane;
import com.nuclear.duck.utils.EntityManager;
import com.nuclear.duck.utils.MissileFactory;

public class Fighter extends Plane {


	public Fighter(TextureRegion texture, MissileFactory mf,
			VertexBufferObjectManager vbom, ArrayList<AttackableEntity> targets,EntityManager em,float life) {
		super(texture, mf, vbom, targets,em,life);
	}

	@Override
	public void update() {
		
		float vectorTarget;
		AttackableEntity closest = null;
		
		try {
			closest = this.getClosest();
			//get Vector to closest target
			vectorTarget = (float)( Math.atan2(this.getCenterY() - closest.getCenterY(),
					this.getCenterX() - closest.getCenterX()) + Math.PI);
		} catch (NoTargetException e) {
			//try to catch itself
			vectorTarget = (float) (this.getSpeedVector() + Math.PI/4.0f);
		}
		
		//Adjust speed
		
		float delta = (float) (this.getSpeedVector() - vectorTarget );
		while (delta < -Math.PI) delta += 2.0f * Math.PI;
		while (delta > Math.PI) delta -= 2.0f * Math.PI;
		
		
		//go over the top of the building
		//if( delta >= 2.0f * Math.PI * 0.002 ) delta = (float) (2.0f * Math.PI * 0.002);
		//if( delta <= - 2.0f * Math.PI * 0.002 ) delta = (float) (-2.0f * Math.PI * 0.002);
		
		this.setSpeedByVector(1.2f, this.getSpeedVector() - delta/200.0f);
		
		//attack
		if(closest != null)
			this.Attack(closest);
		
		//move
		this.updateLocation();
		
	}

	@Override
	protected IEntity getAttackingMissile(AttackableEntity target) {
		return this.mMissileFactory.buildSmartMissile(this.getCenterX(), this.getCenterY(), this.getSpeedVector(), target);
	}

	@Override
	public float getDamage() {
		return 0;
	}


}
