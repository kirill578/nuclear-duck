package com.nuclear.duck.entities;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class LifeBar extends Entity {

	private float mTotalLife;
	private Rectangle mGreen;
	private Rectangle mRed;

	public LifeBar(float totolLife,VertexBufferObjectManager vbom){
		mTotalLife = totolLife;
		this.setZIndex(100);
		mRed = new Rectangle(0, 0, 50, 5, vbom);
		mRed.setColor(Color.RED);
		mGreen = new Rectangle(0, 0, 50, 5, vbom);
		mGreen.setColor(Color.GREEN);
		this.attachChild(mRed);
		this.attachChild(mGreen);
	}
	
	public void update(float life){
		mGreen.setWidth(50.0f*(life/mTotalLife));
	}
	
}
