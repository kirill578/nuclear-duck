package com.nuclear.duck.entities;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.nuclear.duck.GameScene;
import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.AttackingEntity;
import com.nuclear.duck.entities.interfaces.BasicEntity;
import com.nuclear.duck.entities.interfaces.MovableEntity;
import com.nuclear.duck.utils.EntityManager;

public class Missile extends Sprite implements BasicEntity,AttackingEntity,MovableEntity {

	private float mSpeedX;
	private float mSpeedY;
	private AttackableEntity mTarget;
	private boolean mActive;
	private float mSpeedVector;
	private boolean mCanConsume = false;
	
	private long mLifeCycles = 0;
	private long mDieAfter = 100;
	
	private float mSpeed = 4.0f;
	
	private float mTurnSpeed = 0.0f; //per clock
	private float mDamage;
	private EntityManager mEntityManager;
	
	
	/**
	 * 
	 * @param turnSpeed - percentage of the maximum rotation per clock, for instance 0.015f 
	 * @return this
	 */
	public Missile setTurnSpeed(float turnSpeed){
		mTurnSpeed = turnSpeed;
		return this;
	}
	
	/**
	 * 
	 * @param DieAfter - number of clocks after the missile will be self destroyed, for instance 100l
	 * @return this
	 */
	public Missile setDieAfter(long DieAfter){
		mDieAfter = DieAfter;
		return this;
	}
	
	public Missile setSpeed(float speed){
		mSpeed = speed;
		return this;
	}
	
	public Missile(float x, float y, float vector,AttackableEntity target,
			TextureRegion texture, VertexBufferObjectManager vbom,float damage,EntityManager em) {
		super(x , y, texture, vbom);
		
		mTarget = target;
		mActive = true;
		mDamage = damage;
		
		this.setCenterPosition(x, y);
		
		this.setSpeedByVector(mSpeed, vector);
		
		mEntityManager= em;
	}

	@Override
	protected void onManagedUpdate(final float pSecondsElapsed) {
		
		if(mActive){
			
			if( mTurnSpeed != 0.0f ){
				//Adjust speed
				float vectorTarget = (float)( Math.atan2(this.getCenterY() - mTarget.getCenterY(),
						 this.getCenterX() - mTarget.getCenterX()) + Math.PI);
				
				float delta = (float) (this.getSpeedVector() - vectorTarget );
				while (delta < -Math.PI) delta += 2.0f * Math.PI;
				while (delta > Math.PI) delta -= 2.0f * Math.PI;
				
				//go over the top of the building
				if( delta >= 2.0f * Math.PI * mTurnSpeed ) delta = (float) (2.0f * Math.PI * mTurnSpeed );
				if( delta <= - 2.0f * Math.PI * mTurnSpeed ) delta = (float) (-2.0f * Math.PI * mTurnSpeed );
				
				this.setSpeedByVector(mSpeed, this.getSpeedVector() - delta);
			} else {
				this.setSpeedByVector(mSpeed, this.getSpeedVector());
			}
			
			this.updateLocation();
		}
		
		if( ( mLifeCycles++ >= mDieAfter ) || (mActive && this.Attack(mTarget) && mLifeCycles >= 15 ) ){
			mActive = false;
			this.registerEntityModifier(new AlphaModifier(1.0f, 1.0f, 0) {
				

				@Override
				protected void onModifierFinished(IEntity pItem) {
					super.onModifierFinished(pItem);
					Missile.this.setVisible(false);
					Missile.this.mCanConsume = true;
				}

			});

		}
		super.onManagedUpdate(pSecondsElapsed);
		
		if(mCanConsume)
			Missile.this.consume();
	}
	
	public void consume() {
		mEntityManager.removeReferance(this);
		this.setIgnoreUpdate(true);
	}

	public void setSpeed(float x, float y) {
		mSpeedX = x;
		mSpeedY = y;
		this.setRotation((float) Math.toDegrees(Math.atan2(y, x) + Math.PI));
		mSpeedVector = (float) ( Math.toDegrees(Math.atan2(y, x) + Math.PI ));
	}
	
	public void setSpeedByVector(float size, float radian) {
		mSpeedVector = radian;
		mSpeedX = (float) (Math.cos(radian)*size);
		mSpeedY = (float) (Math.sin(radian)*size);
		this.setRotation((float) Math.toDegrees(radian + Math.PI));
	}
	
	public float getSpeedVector() {
		return mSpeedVector;
	}

	public float getXspeed() {
		return mSpeedX;
	}

	public float getYspeed() {
		return mSpeedY;
	}

	public void updateLocation() {
		this.setCenterPosition(this.getCenterX() + this.getXspeed(), this.getCenterY() + this.getYspeed());
	}

	public boolean Attack(AttackableEntity e) {
		
		if( !e.CanBeAttacked(this) )
			return false;
		
		if( this.getTrueDistance(e) > 2.0f ) //TODO change to TRUEDIS
			return false;

		e.BeAttacked(this);
		
		return true;
	}

	public void setCenterPosition(float x, float y) {
		this.setPosition( x - this.getWidth()/2 , y - this.getHeight()/2 );
	}

	public float getCenterX() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[0];
	}

	public float getCenterY() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[1];
	}

	public float getTrueWidth() {
		return this.getWidthScaled();
	}

	public float getTrueHeight() {
		return this.getHeightScaled();
	}

	public float getDistance(BasicEntity e) {
		return MathUtils.distance(this.getCenterX(), this.getCenterY(), e.getCenterX(), e.getCenterY());
	}

	public float getTrueDistance(BasicEntity e) {
		float radious1 = (this.getTrueWidth() + this.getTrueHeight())/4;
		float radious2 = (e.getTrueWidth() + e.getTrueHeight())/4;
		float trueDis = this.getDistance(e) - radious1 - radious2;
		
		if( trueDis <= 0.0f )
			return 0.0f;
		else
			return trueDis;
	}

	@Override
	public float getDamage() {
		return mDamage;
	}

	
}
