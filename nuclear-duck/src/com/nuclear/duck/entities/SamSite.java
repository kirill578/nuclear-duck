package com.nuclear.duck.entities;

import java.util.ArrayList;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.nuclear.duck.LayerManager;
import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.AttackingEntity;
import com.nuclear.duck.entities.interfaces.BasicEntity;
import com.nuclear.duck.entities.interfaces.MortalEntity;
import com.nuclear.duck.utils.EntityManager;
import com.nuclear.duck.utils.MissileFactory;

public class SamSite extends Sprite implements MortalEntity,AttackableEntity,AttackingEntity {

	protected long mLastAttack;
	protected ArrayList<AttackableEntity> mTargets;
	protected MissileFactory mMissileFactory;
	private float mLife;
	
	private EntityManager mEntityManager;
	private LifeBar mLifeBar;
	
	public SamSite(TextureRegion texture,MissileFactory mf,VertexBufferObjectManager vbom,ArrayList<AttackableEntity> targets,EntityManager em,float life){
		super(0 , 0, texture, vbom);
		this.setScale((float) 0.8);
		this.setVisible(true);
		
		this.setCenterPosition(0, 0);
		
		mLifeBar = new LifeBar(life, vbom);
		this.attachChild(mLifeBar);
		
		mLife = life;
		
		mActive = true;
		mCanConsume = false;
		mMissileFactory = mf;
		mTargets = targets;
		
		mEntityManager = em;
	}
	
	protected void singleAttack(AttackableEntity target) {
	}

	public void consume() {
		this.setIgnoreUpdate(true);
		mEntityManager.removeReferance(this);
	}

	public void setCenterPosition(float x, float y) {
		this.setPosition( x - this.getWidth()/2 , y - this.getHeight()/2 );
	}

	public float getCenterX() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[0];
	}

	public float getCenterY() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[1];
	}

	public float getTrueWidth() {
		return this.getWidthScaled();
	}

	public float getTrueHeight() {
		return this.getHeightScaled();
	}
	public float getDistance(BasicEntity e) {
		return MathUtils.distance(this.getCenterX(), this.getCenterY(), e.getCenterX(), e.getCenterY());
	}

	//FIXME
	public float getTrueDistance(BasicEntity e) {
		float radious1 = (this.getTrueWidth() + this.getTrueHeight())/4;
		float radious2 = (e.getTrueWidth() + e.getTrueHeight())/4;
		float trueDis = this.getDistance(e) - radious1 - radious2;
		
		if( trueDis <= 0.0f )
			return 0.0f;
		else
			return trueDis;
	}

	public boolean Attack(AttackableEntity e) {
		
		if( System.currentTimeMillis() - mLastAttack < 2300 )
			return false;
		
		if( !e.CanBeAttacked(this) )
			return false;
		
		if( this.getTrueDistance(e) > 450.0f )
			return false;
		
		mLastAttack = System.currentTimeMillis();
		
		float vectorTarget = (float)( Math.atan2(this.getCenterY() - e.getCenterY(),
				 this.getCenterX() - e.getCenterX()) + Math.PI);
		
		LayerManager.missileLayer.attachChild(mMissileFactory.buildDeafult(getCenterX(), getCenterY(), vectorTarget, e));
		
		return true;
	}

	public boolean CanBeAttacked(AttackingEntity e) {
		return mActive;
	}

	public void BeAttacked(AttackingEntity e) {
		setLife(getLife() - e.getDamage());
	}

	public void setLife(float life) {
		mLife = life;
		mLifeBar.update(life);
	}

	public float getLife() {
		return mLife;
	}
	
	private boolean mCanConsume;
	protected boolean mActive;
	
	@Override
	protected void onManagedUpdate(final float pSecondsElapsed) {
		
		this.sortChildren();
		
		if( mLife <= 0.0f ){
			mActive = false;
			this.registerEntityModifier(new AlphaModifier(1.0f, 1.0f, 0) {

				@Override
				protected void onModifierFinished(IEntity pItem) {
					super.onModifierFinished(pItem);
					SamSite.this.setVisible(false);
					SamSite.this.mCanConsume = true;
				}

			});
			
		}
		
		if(mActive){
			AttackableEntity closest = null;
			
			for (AttackableEntity e : mTargets)
				if( closest == null || closest.getTrueDistance(this) > e.getTrueDistance(this) )
					closest = e;
			
			if( closest != null )
				this.Attack(closest);
		}
		
		if(mCanConsume)
			this.consume();
		
		super.onManagedUpdate(pSecondsElapsed);
	}

	@Override
	public float getDamage() {
		return 0;
	}


}
