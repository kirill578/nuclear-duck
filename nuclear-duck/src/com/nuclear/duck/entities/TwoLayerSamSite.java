package com.nuclear.duck.entities;

import java.util.ArrayList;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.LayerManager;
import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.utils.EntityManager;
import com.nuclear.duck.utils.MissileFactory;

public class TwoLayerSamSite extends SamSite {

	public static final float MINIMAL_ATTACK_DISTANCE = 450.0f;
	private Sprite mTopLayer;
	private float mRotation;
	private float mDesiredRotation;
	private AttackableEntity mActiveTarget;
	private Sprite mShadowLayer;
	
	public TwoLayerSamSite(TextureRegion texture_base,TextureRegion texture_top,TextureRegion texture_shadow, MissileFactory mf,
			VertexBufferObjectManager vbom,
			ArrayList<AttackableEntity> targets, EntityManager em, float life) {
		super(texture_base, mf, vbom, targets, em, life);
		
		mShadowLayer = mTopLayer = new Sprite(0 -9, -20 +3, texture_shadow, vbom); 
		mTopLayer = new Sprite(0, -20, texture_top, vbom);
		this.attachChild(mShadowLayer);
		this.attachChild(mTopLayer);
		mRotation = 0;
		
	}
	
	

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);
		if(mActive)
			this.sigleStepToDesiredRotation();
	}


	public final static float SINGLE_STEP_ROTATION = 0.03f;
	public final static float WRONG_ROTATION_MISSILE_LAUNCH_THOLD = (float) (Math.PI/2.0f);
	
	private void sigleStepToDesiredRotation() {
		if(Math.abs(mDesiredRotation-mRotation) > SINGLE_STEP_ROTATION){
			if( mDesiredRotation - mRotation < 0 )
				mRotation -= SINGLE_STEP_ROTATION;
			else
				mRotation += SINGLE_STEP_ROTATION;
		} else {
			mRotation = mDesiredRotation;
		}
			
		mTopLayer.setRotation((float) Math.toDegrees(mRotation + Math.PI/2.0f));
		mShadowLayer.setRotation((float) Math.toDegrees(mRotation + Math.PI/2.0f));
	}

	@Override
	public boolean Attack(AttackableEntity e) {
		
		if(mActiveTarget != null && mActiveTarget.CanBeAttacked(this) && this.getTrueDistance(mActiveTarget) < MINIMAL_ATTACK_DISTANCE/1.5f )
			e = mActiveTarget;
		
		//rotate missiles to target
		float vectorTarget = (float)( Math.atan2(this.getCenterY() - e.getCenterY(),
				 this.getCenterX() - e.getCenterX()) + Math.PI);
		
		mDesiredRotation = vectorTarget;
		
		if( System.currentTimeMillis() - mLastAttack < 2300 )
			return false;
		
		if( !e.CanBeAttacked(this) )
			return false;
		
		if( this.getTrueDistance(e) > MINIMAL_ATTACK_DISTANCE )
			return false;
		
		mLastAttack = System.currentTimeMillis();
		
		if(Math.abs(mRotation-mDesiredRotation) <= WRONG_ROTATION_MISSILE_LAUNCH_THOLD){
			LayerManager.missileLayer.attachChild(mMissileFactory.buildDeafult(getCenterX(), getCenterY(), mRotation, e));
			mActiveTarget = e;
		}
		
		return true;
	}

	

}
