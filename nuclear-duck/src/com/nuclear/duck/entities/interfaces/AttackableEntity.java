package com.nuclear.duck.entities.interfaces;

public interface AttackableEntity extends BasicEntity {

	public abstract boolean CanBeAttacked(AttackingEntity e);
	
	public abstract void BeAttacked(AttackingEntity e);
	
}
