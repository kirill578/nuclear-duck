package com.nuclear.duck.entities.interfaces;

public interface AttackingEntity extends BasicEntity {

	public abstract boolean Attack(AttackableEntity e);
	
	public abstract float getDamage();
	
}
