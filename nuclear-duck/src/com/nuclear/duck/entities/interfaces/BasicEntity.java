package com.nuclear.duck.entities.interfaces;

public interface BasicEntity {

	public abstract float getCenterX();
	
	public abstract float getCenterY();
	
	public abstract void setCenterPosition(float x,float y);
	
	public abstract void consume();
	
	public abstract float getTrueWidth();
	
	public abstract float getTrueHeight();
	
	public abstract float getDistance(BasicEntity e);
	
	public abstract float getTrueDistance(BasicEntity e);
		
}
