package com.nuclear.duck.entities.interfaces;

public interface ButtonPressListener {

	public abstract void onButtonPressed();
	
}
