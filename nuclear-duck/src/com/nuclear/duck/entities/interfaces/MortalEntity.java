package com.nuclear.duck.entities.interfaces;


public interface MortalEntity extends BasicEntity {

	public abstract void setLife(float life);
	
	public abstract float getLife();
	
}
