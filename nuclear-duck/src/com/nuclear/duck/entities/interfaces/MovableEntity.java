package com.nuclear.duck.entities.interfaces;


public interface MovableEntity extends BasicEntity {

	public abstract float getXspeed();
	
	public abstract float getYspeed();
	
	public abstract void setSpeed(float x,float y);
	
	public abstract void setSpeedByVector(float size,float radian);
	
	public abstract void updateLocation();
	
}
