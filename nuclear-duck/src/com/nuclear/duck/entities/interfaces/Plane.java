package com.nuclear.duck.entities.interfaces;

import java.util.ArrayList;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.nuclear.duck.GameHUD;
import com.nuclear.duck.LayerManager;
import com.nuclear.duck.entities.SamSite;
import com.nuclear.duck.utils.EntityManager;
import com.nuclear.duck.utils.MissileFactory;

import android.util.Log;

public abstract class Plane extends Sprite implements MortalEntity,MovableEntity,AttackableEntity,AttackingEntity {
	
	private float mSpeedX;
	private float mSpeedY;
	private float mSpeedTotal = 4.0f;
	
	private float mSpeedVector;
	
	private long mLastAttack = 0;
	private ArrayList<AttackableEntity> mTargets;
	
	protected MissileFactory mMissileFactory;
	
	protected float mMinimumAttackDistance = 250.0f;
	protected Long mAttackInterval = 800l;
	private EntityManager mEntityManager;
	private float mLife;
	private boolean mActive;
	

	public Plane(TextureRegion texture,MissileFactory mf,VertexBufferObjectManager vbom,ArrayList<AttackableEntity> targets,EntityManager em,float life){
		super(0 , 0, texture, vbom);
		
		this.setScale((float) 0.25);
		this.setVisible(true);
		
		this.setCenterPosition(0, 0);
		
		mTargets = targets;
		mMissileFactory = mf;
		mEntityManager = em;
		mLife = life;
		mActive = true;
		mCanConsume = false;
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,float pTouchAreaLocalX, float pTouchAreaLocalY) {
		this.setCenterPosition(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
		return true;
	}
	
	private boolean mCanConsume;
	
	@Override
	protected void onManagedUpdate(final float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);
		if(mActive){
			this.update();
			if( mLife <= 0.0f ){
				mActive = false;
				this.registerEntityModifier(new AlphaModifier(1.0f, 1.0f, 0) {
	
					@Override
					protected void onModifierFinished(IEntity pItem) {
						super.onModifierFinished(pItem);
						Plane.this.setVisible(false);
						Plane.this.mCanConsume = true;
					}
	
				});
				
			}
		}
		
		if(mCanConsume)
			this.consume();
	}
	
	public abstract void update();
	
	public AttackableEntity getClosest() throws NoTargetException {
		AttackableEntity closest;
		try {
			closest = this.mTargets.get(0);
		} catch (Exception e){
			throw new NoTargetException();
		}
		for (AttackableEntity e : this.mTargets) {
			if(MathUtils.distance(this.getCenterX(), this.getCenterY(),
					e.getCenterX(),e.getCenterY()) < MathUtils.distance(this.getCenterX(), this.getCenterY(),
							closest.getCenterX(),closest.getCenterY()) )
			closest = e;
		}
		
		return closest;
	}
	

	public float getTouchTreshold() {
		return 25.0f;
	}

	public float getMinmumAttackDistance() {
		return 250.0f;
	}

	public long getAttackInterval() {
		return 800;
	}

	public void setVector(float radian) {
		mSpeedVector = radian;
		mSpeedX = (float) (Math.cos(radian)*mSpeedTotal);
		mSpeedY = (float) (Math.sin(radian)*mSpeedTotal);
		this.setRotation((float) Math.toDegrees(radian + Math.PI*3.0f/2.0f));
	}

	public void consume() {
		mEntityManager.removeReferance(this);
		this.setIgnoreUpdate(true);
	}

	public void setSpeed(float x, float y) {
		mSpeedX = x;
		mSpeedY = y;
		this.setRotation((float) Math.toDegrees(Math.atan2(y, x) + Math.PI));
		mSpeedVector = (float) Math.toDegrees(Math.atan2(y, x) + Math.PI*3.0f/2.0f);
	}
	
	public void setSpeedByVector(float size, float radian) {
		mSpeedVector = radian;
		mSpeedX = (float) (Math.cos(radian)*size);
		mSpeedY = (float) (Math.sin(radian)*size);
		this.setRotation((float) Math.toDegrees(radian + Math.PI*3.0f/2.0f));
	}
	
	public float getSpeedVector() {
		return mSpeedVector;
	}

	public float getXspeed() {
		return mSpeedX;
	}

	public float getYspeed() {
		return mSpeedY;
	}

	public void updateLocation() {
		this.setCenterPosition(this.getCenterX() + this.getXspeed(), this.getCenterY() + this.getYspeed());
	}

	public boolean Attack(AttackableEntity e) {
		
		if( System.currentTimeMillis() - mLastAttack < mAttackInterval )
			return false;
		
		if( !e.CanBeAttacked(this) )
			return false;
		
		if( this.getTrueDistance(e) > mMinimumAttackDistance )
			return false;

		
		mLastAttack = System.currentTimeMillis();
		
		//this.getParent().attachChild(getAttackingMissile(e));
		
		LayerManager.groundLayer.attachChild(getAttackingMissile(e));
		
		return true;
	}
	
	protected abstract IEntity getAttackingMissile(AttackableEntity target);

	public void setCenterPosition(float x, float y) {
		this.setPosition( x - this.getWidth()/2 , y - this.getHeight()/2 );
	}

	public float getCenterX() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[0];
	}

	public float getCenterY() {
		float[] objCenterPos = new float[2];
		this.getSceneCenterCoordinates(objCenterPos);
		return objCenterPos[1];
	}

	public float getTrueWidth() {
		return this.getWidthScaled();
	}

	public float getTrueHeight() {
		return this.getHeightScaled();
	}

	public float getDistance(BasicEntity e) {
		return MathUtils.distance(this.getCenterX(), this.getCenterY(), e.getCenterX(), e.getCenterY());
	}

	public float getTrueDistance(BasicEntity e) {
		float radious1 = (this.getTrueWidth() + this.getTrueHeight())/4;
		float radious2 = (e.getTrueWidth() + e.getTrueHeight())/4;
		float trueDis = this.getDistance(e) - radious1 - radious2;
		
		if( trueDis <= 0.0f )
			return 0.0f;
		else
			return trueDis;
	}

	public boolean CanBeAttacked(AttackingEntity e) {
		return mActive; //TODO
	}

	public void BeAttacked(AttackingEntity e) {
		if( GameHUD.getInstance() != null ) GameHUD.getInstance().incrementScore(); //FIXME
		
		mLife -= e.getDamage();
	}

	public void setLife(float life) {
		mLife = life;
	}

	public float getLife() {
		return mLife;
	}
	
}
