package com.nuclear.duck.entities.interfaces;

public interface TouchListener {
	
	public abstract void onTouch(float x,float y);

}
