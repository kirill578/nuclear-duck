package com.nuclear.duck.utils;

import java.util.ArrayList;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.TextureProvider;
import com.nuclear.duck.entities.Bomber;
import com.nuclear.duck.entities.Fighter;
import com.nuclear.duck.entities.SamSite;
import com.nuclear.duck.entities.interfaces.AttackableEntity;
import com.nuclear.duck.entities.interfaces.Plane;

public class BomberFactory {
	
	private TextureRegion mTexture;
	private VertexBufferObjectManager vbom;
	private MissileFactory mMissileFactory;
	
	private ArrayList<AttackableEntity> mTargets;
	private ArrayList<AttackableEntity> mGroup;
	private EntityManager mEntityManager;
	private float mLife;

	/**
	 * 
	 * @param texture - texture of the fighter
	 * @param vbom - VertexBufferObjectManager
	 * @param mf - MissileFactory which will be used to create a missile
	 * @param targets - target on which the plane will aim its missiles
	 * @param group - Adds the bomber to a group, null can be passed
	 */
	public BomberFactory(TextureRegion texture,VertexBufferObjectManager vbom,MissileFactory mf,ArrayList<AttackableEntity> targets,ArrayList<AttackableEntity> group,EntityManager em,float life){
		mTexture = texture;
		this.vbom = vbom;
		mMissileFactory = mf;
		mTargets = targets;
		mGroup = group;
		mEntityManager = em;
		mLife = life;
	}
	
	public Bomber build(int centerX,int centerY){
		Bomber bomber = new Bomber(mTexture,mMissileFactory,vbom,mTargets,mEntityManager,mLife);
		bomber.setCenterPosition(centerX,centerY);
		
		if( mGroup != null )
			mGroup.add(bomber);
		
		return bomber;
	}

}
