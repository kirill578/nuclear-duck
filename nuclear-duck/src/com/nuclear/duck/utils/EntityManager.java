package com.nuclear.duck.utils;

import java.util.ArrayList;
import java.util.Collection;

import org.andengine.engine.Engine;
import org.andengine.entity.Entity;

public class EntityManager {

	private Engine mEngine;
	private ArrayList<Collection> mCollection;

	public EntityManager(Engine engine) {
		mCollection = new ArrayList<Collection>();
		mEngine = engine;
	}

	public void addReferanceCollection(Collection coll) {
		mCollection.add(coll);
	}

	public void removeReferance(final Entity entity) {

		for (Collection col : mCollection)
			col.remove(entity);

		mEngine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				entity.detachSelf();
			}
		});

	}

}
