package com.nuclear.duck.utils;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.entities.Missile;
import com.nuclear.duck.entities.interfaces.AttackableEntity;

public class MissileFactory {
	
	private TextureRegion mTexture;
	private VertexBufferObjectManager vbom;
	private EntityManager mEntityManger;
	
	public MissileFactory(TextureRegion texture, VertexBufferObjectManager vbom,EntityManager em){
		mTexture = texture;
		this.vbom = vbom;
		mEntityManger = em;
	}
	
	public Missile buildSmartMissile(float x,float y,float initialVector,AttackableEntity target){
		Missile missile = new Missile(x, y, initialVector, target, mTexture, vbom,1.0f,mEntityManger)
							.setSpeed(4.0f).setDieAfter(100).setTurnSpeed(0.015f);
		return missile;
	}
	
	public Missile buildBomb(float x,float y,float initialVector,AttackableEntity target){
		Missile missile = new Missile(x, y, initialVector, target, mTexture, vbom,0.5f,mEntityManger)
							.setSpeed(1.0f).setDieAfter(20);
		return missile;
	}
	
	public Missile buildDeafult(float x,float y,float initialVector,AttackableEntity target){
		Missile missile = new Missile(x, y, initialVector, target, mTexture, vbom,1.0f,mEntityManger);
		return missile;
	}
	
	public Missile build(float x,float y,float initialVector,float speed,long lifeTime,AttackableEntity target, float damage){
		Missile missile = new Missile(x, y, initialVector, target, mTexture, vbom,damage,mEntityManger)
							.setSpeed(speed).setDieAfter(lifeTime);
		return missile;
	}
	
	

}
