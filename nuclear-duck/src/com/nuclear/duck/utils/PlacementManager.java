package com.nuclear.duck.utils;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.Sprite;

import com.nuclear.duck.entities.interfaces.TouchListener;

public class PlacementManager implements TouchListener {
	
	private boolean isActive = false;
	private Entity mLayer;
	private SamFactory mSamFactory;
	
	public PlacementManager(Entity layer,SamFactory samFactory){
		mLayer = layer;
		mSamFactory = samFactory;
	}
	
	public void activate(){
		isActive = true;
	}
	
	public void diactivate(){
		isActive = false;
	}

	@Override
	public void onTouch(float x, float y) {
		if(isActive){
			
			int size = mLayer.getChildCount();
			for(int i = 0 ; i < size ; i++ ){
				Sprite e = (Sprite) mLayer.getChildByIndex(i);
				float tempX = e.getX() + e.getScaleCenterX();
				float tempY = e.getY() + e.getScaleCenterY();
				float tempW = e.getWidth();
				float tempH = e.getHeight();
				if( tempX - tempW/2.0f <= x && tempX + tempW/2.0f >= x && tempY - tempH/2.0f <= y && tempY + tempH/2.0f >= y )
					return;
				
				//if( tempX <= x && tempX + tempW >= x && tempY <= y && tempY + tempH >= y )
			}
			
			mLayer.attachChild(mSamFactory.build((int) x,(int) y));
			this.diactivate();
		}
	}

}
