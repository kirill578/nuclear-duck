package com.nuclear.duck.utils;

import java.util.ArrayList;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.TextureProvider;
import com.nuclear.duck.entities.SamSite;
import com.nuclear.duck.entities.interfaces.AttackableEntity;

public class SamFactory {
	
	protected TextureRegion mTexture;
	protected VertexBufferObjectManager vbom;
	protected MissileFactory mMissileFactory;
	
	protected ArrayList<AttackableEntity> mTargets;
	protected ArrayList<AttackableEntity> mGroup;
	
	protected EntityManager mEntityManager;
	protected float mLife;

	/**
	 * 
	 * @param texture - texture of the sam site
	 * @param vbom - VertexBufferObjectManager
	 * @param mf - MissileFactory which will be used to create a missile
	 * @param targets - target on which the sam will aim its missiles
	 * @param group - Adds the SamSite to a group, null can be passed
	 */
	public SamFactory(TextureRegion texture,VertexBufferObjectManager vbom,MissileFactory mf,ArrayList<AttackableEntity> targets,ArrayList<AttackableEntity> group,EntityManager entityManager,float life){
		mTexture = texture;
		this.vbom = vbom;
		mMissileFactory = mf;
		mTargets = targets;
		mGroup = group;
		mEntityManager = entityManager;
		mLife = life;
	}
	
	public SamSite build(int centerX,int centerY){
		SamSite samSite = new SamSite(mTexture,mMissileFactory,vbom, mTargets,mEntityManager,mLife);
		samSite.setCenterPosition(centerX,centerY);
		
		if( mGroup != null )
			mGroup.add(samSite);
		
		return samSite;
	}

}
