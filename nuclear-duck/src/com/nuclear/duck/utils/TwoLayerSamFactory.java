package com.nuclear.duck.utils;

import java.util.ArrayList;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.nuclear.duck.entities.SamSite;
import com.nuclear.duck.entities.TwoLayerSamSite;
import com.nuclear.duck.entities.interfaces.AttackableEntity;

public class TwoLayerSamFactory extends SamFactory {

	private TextureRegion mTextureTop;
	private TextureRegion mTextureShadow;

	public TwoLayerSamFactory(TextureRegion texture_base,TextureRegion texture_top,TextureRegion texture_shadow,
			VertexBufferObjectManager vbom, MissileFactory mf,
			ArrayList<AttackableEntity> targets,
			ArrayList<AttackableEntity> group, EntityManager entityManager,
			float life) {
		super(texture_base, vbom, mf, targets, group, entityManager, life);
		mTextureTop = texture_top;
		mTextureShadow = texture_shadow;
	}

	@Override
	public SamSite build(int centerX, int centerY) {
		SamSite samSite = new TwoLayerSamSite(mTexture,mTextureTop,mTextureShadow,mMissileFactory,vbom, mTargets,mEntityManager,mLife);
		samSite.setCenterPosition(centerX,centerY);
		
		if( mGroup != null )
			mGroup.add(samSite);
		
		return samSite;
	}
	
	

}
